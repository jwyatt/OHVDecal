﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace OHVDecal.Models
{
    public partial class OHVDecalContext : DbContext
    {
        private IConfiguration Configuration { get; set; }

        public OHVDecalContext()
        {
        }

        public OHVDecalContext(IConfiguration configuration)
        {
            Configuration = configuration;         
        }

        //public OHVDecalContext(DbContextOptions<OHVDecalContext> options)
        //    : base(options)
        //{
        //}

        public virtual DbSet<LogInfo> LogInfo { get; set; }     
        public virtual DbSet<Reply> Reply { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("OHVDecalContextConnection"));
                //Dev
                //optionsBuilder.UseSqlServer("Server=gfaw-sql1s;Database=OHVDecal;Trusted_Connection=True;");
                //PROD
                //optionsBuilder.UseSqlServer("Server=gfaw-sql1p;Database=OHVDecal;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

      
            modelBuilder.Entity<Reply>(entity =>
            {
                entity.Property(e => e.AuthAmount)
                    .HasColumnName("auth_amount")
                    .HasMaxLength(255);

                entity.Property(e => e.AuthAvsCode)
                    .HasColumnName("auth_avs_code")
                    .HasMaxLength(255);

                entity.Property(e => e.AuthAvsCodeRaw)
                    .HasColumnName("auth_avs_code_raw")
                    .HasMaxLength(255);

                entity.Property(e => e.AuthCode)
                    .HasColumnName("auth_code")
                    .HasMaxLength(255);

                entity.Property(e => e.AuthResponse)
                    .HasColumnName("auth_response")
                    .HasMaxLength(255);

                entity.Property(e => e.AuthTime)
                    .HasColumnName("auth_time")
                    .HasMaxLength(255);

                entity.Property(e => e.AuthTransRefNo)
                    .HasColumnName("auth_trans_ref_no")
                    .HasMaxLength(255);

                entity.Property(e => e.BillTransRefNo)
                    .HasColumnName("bill_trans_ref_no")
                    .HasMaxLength(255);

                entity.Property(e => e.Decision)
                    .HasColumnName("decision")
                    .HasMaxLength(255);

                entity.Property(e => e.MerchantDefinedData1)
                    .HasColumnName("merchant_defined_data1")
                    .HasMaxLength(100);

                entity.Property(e => e.MerchantDefinedData2)
                    .HasColumnName("merchant_defined_data2")
                    .HasMaxLength(100);

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasMaxLength(255);

                entity.Property(e => e.ReasonCode)
                    .HasColumnName("reason_code")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqAccessKey)
                    .HasColumnName("req_access_key")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqAmount)
                    .HasColumnName("req_amount")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToAddressCity)
                    .HasColumnName("req_bill_to_address_city")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToAddressCountry)
                    .HasColumnName("req_bill_to_address_country")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToAddressLine1)
                    .HasColumnName("req_bill_to_address_line1")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToAddressLine2)
                    .HasColumnName("req_bill_to_address_line2")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToAddressPostalCode)
                    .HasColumnName("req_bill_to_address_postal_code")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToAddressState)
                    .HasColumnName("req_bill_to_address_state")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToEmail)
                    .HasColumnName("req_bill_to_email")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToForename)
                    .HasColumnName("req_bill_to_forename")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToPhone)
                    .HasColumnName("req_bill_to_phone")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqBillToSurname)
                    .HasColumnName("req_bill_to_surname")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqCardExpiryDate)
                    .HasColumnName("req_card_expiry_date")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqCardNumber)
                    .HasColumnName("req_card_number")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqCardType)
                    .HasColumnName("req_card_type")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqCardTypeSelectionIndicator)
                    .HasColumnName("req_card_type_selection_indicator")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqConsumerId)
                    .HasColumnName("req_consumer_id")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqCurrency)
                    .HasColumnName("req_currency")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqLocale)
                    .HasColumnName("req_locale")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqPaymentMethod)
                    .HasColumnName("req_payment_method")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqProfileId)
                    .HasColumnName("req_profile_id")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqReferenceNumber)
                    .HasColumnName("req_reference_number")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToAddressCity)
                    .HasColumnName("req_ship_to_address_city")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToAddressCountry)
                    .HasColumnName("req_ship_to_address_country")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToAddressLine1)
                    .HasColumnName("req_ship_to_address_line1")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToAddressLine2)
                    .HasColumnName("req_ship_to_address_line2")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToAddressPostalCode)
                    .HasColumnName("req_ship_to_address_postal_code")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToAddressState)
                    .HasColumnName("req_ship_to_address_state")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToForename)
                    .HasColumnName("req_ship_to_forename")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToPhone)
                    .HasColumnName("req_ship_to_phone")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqShipToSurname)
                    .HasColumnName("req_ship_to_surname")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqTransactionType)
                    .HasColumnName("req_transaction_type")
                    .HasMaxLength(255);

                entity.Property(e => e.ReqTransactionUuid)
                    .HasColumnName("req_transaction_uuid")
                    .HasMaxLength(255);

                entity.Property(e => e.RequestToken)
                    .HasColumnName("request_token")
                    .HasMaxLength(255);

                entity.Property(e => e.SignedDateTime)
                    .HasColumnName("signed_date_time")
                    .HasMaxLength(255);

                entity.Property(e => e.TransactionId)
                    .HasColumnName("transaction_id")
                    .HasMaxLength(255);
            });
        }

    }
}
