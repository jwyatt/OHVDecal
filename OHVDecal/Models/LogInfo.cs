﻿using System;
using System.Collections.Generic;

namespace OHVDecal.Models
{
    public partial class LogInfo
    {
        public int Id { get; set; }
        public string Info { get; set; }
    }
}
