﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace OHVDecal.Models
{
    public partial class PaymentsContext : DbContext
    {
        private IConfiguration Configuration { get; set; }

        public PaymentsContext()
        {
        }

        public PaymentsContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        //public PaymentsContext(DbContextOptions<PaymentsContext> options)
        //    : base(options)
        //{
        //}

        public virtual DbSet<Items> Items { get; set; }
        public virtual DbSet<Sale> Sale { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("PaymentsContextConnection"));
                //DEV
                //optionsBuilder.UseSqlServer("Server=gfaw-sql1s;Database=Payments;Trusted_Connection=True;");
                //PROD
                //optionsBuilder.UseSqlServer("Server=gfaw-sql1p;Database=Payments;Trusted_Connection=True;");

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Items>(entity =>
            {
                //entity.HasKey(e => e.Id)
                //    .ForSqlServerIsClustered(false);
                entity.HasKey(e => e.Id)
                   .IsClustered(false);

                entity.ToTable("Items", "dbo");

                entity.HasIndex(e => e.Agency)
                    .HasName("IDX_AGENCY_NU_C")
                    .IsClustered();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Agency)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.AgencyObject)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.CompObject)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.CreateDateTime).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Gaoindex)
                    .IsRequired()
                    .HasColumnName("GAOIndex")
                    .HasMaxLength(5);

                entity.Property(e => e.Pca)
                    .IsRequired()
                    .HasColumnName("PCA")
                    .HasMaxLength(5);

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.SaleId).HasColumnName("SaleID");

                entity.Property(e => e.UnitCost).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.ToTable("Sale", "dbo");

                entity.HasIndex(e => new { e.SaleId, e.MerchantReferenceCode })
                    .HasName("IDX_SALEID_MERCHANTREFERENCECODE_NU_NC");

                entity.HasIndex(e => new { e.DocAgency, e.MerchantReferenceCode, e.SaleId })
                    .HasName("IDX_DOCAGENCY_MERCHANTREFERENCECODE_SALEID_NU_NC");

                entity.HasIndex(e => new { e.SaleId, e.Sent, e.BatchAgency })
                    .HasName("IDX_BATCHAGENCY_NU_NC");

                entity.HasIndex(e => new { e.AuthReconciliationId, e.CaptureReconciliationId, e.SaleId, e.MerchantReferenceCode })
                    .HasName("IDX_SALEID_MERCHANTREFERENCECODE_AUTHRECONCILIATIONID_CAPTURERECONCILIATIONID_NU_NC");

                entity.HasIndex(e => new { e.Date, e.Sent, e.BatchAgency, e.DocAgency })
                    .HasName("IDX_SENT_BATCHAGENCY_DOCAGENCY_NU_NC");

                entity.HasIndex(e => new { e.AuthReconciliationId, e.CaptureReconciliationId, e.TotalPurchase, e.BatchAgency, e.MerchantReferenceCode })
                    .HasName("IDX_BATCHAGENCY_MERCHANTREFERENCECODE_NU_NC");

                entity.Property(e => e.SaleId).HasColumnName("SaleID");

                entity.Property(e => e.AuthReconciliationId)
                    .IsRequired()
                    .HasColumnName("AuthReconciliationID")
                    .HasMaxLength(60);

                entity.Property(e => e.BatchAgency)
                    .IsRequired()
                    .HasMaxLength(3);

                entity.Property(e => e.CaptureReconciliationId)
                    .HasColumnName("CaptureReconciliationID")
                    .HasMaxLength(60);

                entity.Property(e => e.DocAgency).HasMaxLength(3);

                entity.Property(e => e.MerchantReferenceCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OrderNumber).HasMaxLength(16);

                entity.Property(e => e.RequestId)
                    .IsRequired()
                    .HasColumnName("RequestID")
                    .HasMaxLength(26);

                entity.Property(e => e.TotalPurchase).HasColumnType("smallmoney");
            });
        }
    }
}
