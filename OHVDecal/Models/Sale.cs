﻿using System;
using System.Collections.Generic;

namespace OHVDecal.Models
{
    public partial class Sale
    {
        public int SaleId { get; set; }
        public DateTime Date { get; set; }
        public string AuthReconciliationId { get; set; }
        public string CaptureReconciliationId { get; set; }
        public string RequestId { get; set; }
        public string MerchantReferenceCode { get; set; }
        public decimal TotalPurchase { get; set; }
        public string BatchAgency { get; set; }
        public string OrderNumber { get; set; }
        public string DocAgency { get; set; }
        public bool Sent { get; set; }
    }
}
