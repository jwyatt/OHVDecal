﻿using System;
using System.Collections.Generic;

namespace OHVDecal.Models
{
    public partial class Reply
    {
        public int Id { get; set; }
        public string ReqLocale { get; set; }
        public string ReqCardTypeSelectionIndicator { get; set; }
        public string AuthTransRefNo { get; set; }
        public string ReqCardExpiryDate { get; set; }
        public string AuthAmount { get; set; }
        public string AuthResponse { get; set; }
        public string BillTransRefNo { get; set; }
        public string ReqPaymentMethod { get; set; }
        public string AuthTime { get; set; }
        public string TransactionId { get; set; }
        public string ReqCardType { get; set; }
        public string ReqConsumerId { get; set; }
        public string AuthAvsCode { get; set; }
        public string AuthCode { get; set; }
        public string ReasonCode { get; set; }
        public string ReqCardNumber { get; set; }
        public string RequestToken { get; set; }
        public string SignedDateTime { get; set; }
        public string ReqProfileId { get; set; }
        public string ReqBillToSurname { get; set; }
        public string ReqBillToPhone { get; set; }
        public string ReqBillToAddressCountry { get; set; }
        public string ReqBillToEmail { get; set; }
        public string ReqBillToAddressLine2 { get; set; }
        public string ReqBillToAddressCity { get; set; }
        public string ReqBillToForename { get; set; }
        public string ReqBillToAddressPostalCode { get; set; }
        public string ReqBillToAddressState { get; set; }
        public string ReqShipToAddressPostalCode { get; set; }
        public string ReqShipToAddressState { get; set; }
        public string ReqShipToAddressCity { get; set; }
        public string ReqShipToAddressLine1 { get; set; }
        public string ReqShipToAddressCountry { get; set; }
        public string ReqShipToPhone { get; set; }
        public string ReqShipToAddressLine2 { get; set; }
        public string ReqBillToAddressLine1 { get; set; }
        public string ReqShipToSurname { get; set; }
        public string ReqShipToForename { get; set; }
        public string ReqAmount { get; set; }
        public string AuthAvsCodeRaw { get; set; }
        public string ReqCurrency { get; set; }
        public string Decision { get; set; }
        public string Message { get; set; }
        public string ReqTransactionUuid { get; set; }
        public string ReqTransactionType { get; set; }
        public string ReqAccessKey { get; set; }
        public string ReqReferenceNumber { get; set; }
        public string MerchantDefinedData1 { get; set; }
        public string MerchantDefinedData2 { get; set; }
        public DateTime TagSentDate { get; set; }
        public bool Processed { get; set; }
    }
}
