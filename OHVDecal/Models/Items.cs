﻿using System;
using System.Collections.Generic;

namespace OHVDecal.Models
{
    public partial class Items
    {
        public int SaleId { get; set; }
        public string Pca { get; set; }
        public string CompObject { get; set; }
        public string AgencyObject { get; set; }
        public string Gaoindex { get; set; }
        public string Agency { get; set; }
        public string Description { get; set; }
        public string ProductCode { get; set; }
        public decimal UnitCost { get; set; }
        public int Quantity { get; set; }
        public DateTime? CreateDateTime { get; set; }
        public int Id { get; set; }
    }
}
