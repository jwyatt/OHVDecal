﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OHVDecal.ViewModels
{
    public class RequiredCheckbox
    {
        [Display(Name = "I Agree to the Terms and Conditions.")]
        [Range(typeof(bool), "true", "true", ErrorMessage = " You must agree to continue.")]
        public bool agree { get; set; }
    }
}
