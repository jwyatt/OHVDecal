﻿using Microsoft.Extensions.Configuration;

namespace OHVDecal.Helper
{
    public static class ConfigurationHelper
    {
        public static IConfiguration config;

        public static void SetConfiguration(IConfiguration configuration)
        {
            config = configuration;
        }
        public static string GetSecretKey()
        {
            return config.GetValue<string>("SecretKey");
        }
    }
}
