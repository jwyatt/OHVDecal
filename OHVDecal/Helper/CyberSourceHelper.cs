﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace OHVDecal.Helper
{
    public class CyberSourceHelper
    {
        public CyberSourceHelper()
        {
            SecretKey = string.Empty;
            AccessKey = string.Empty;
            ProfileId = string.Empty;
            PayAction = string.Empty;
            UpdateAction = string.Empty;
            TransactionType = string.Empty;
        }

        public string SecretKey { get; set; }
        public string AccessKey { get; set; }
        public string ProfileId { get; set; }
        public string PayAction { get; set; }
        public string UpdateAction { get; set; }
        public string TransactionType { get; set; }
    }
}
