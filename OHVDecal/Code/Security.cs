﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OHVDecal.Helper;

namespace OHVDecal.Code
{
    public class Security
    {
        //dev
        //private const string SECRET_KEY = "b3d3af0ffc9d4a7b84b8951cc8bec606c08f92ece141462f940be931502e92c65008414b43d541aa95e97f0c55d9688ff22368b5c6f0431dbbd02b068c47837ac3f5efaf2b6640e29b790926c4fdead1ac44ba375931414d8c128eab250d91a5b3acff591b684b2f9c86f9e018ce6408c3d662b47179435da409eb4eb9e58b19";

        //prod
        //private const string SECRET_KEY = "1ea3b187c01b43238dbd6d91dde643da00a34ffc827541948e67d94a707a71112a62ea17a7194a4882f244f81d10ec5b70cbb9ed26424d0ab5dc4b5b051b2cfc6a6be243e62743deb4ad0234c8f946938e59053c363e43a483937e1b6422e9b6b400a61e2e914ddea4a13558ac1051db78c9b0dc146448b8824c09a5c5c4b2a0";

        public static string Sign(IDictionary<string, string> paramsArray)
        {
            string SecretKey = paramsArray["secret_key"].ToString();
            return Sign(BuildDataToSign(paramsArray), SecretKey);
        }

        private static string Sign(String data, String secretKey)
        {
            UTF8Encoding encoding = new System.Text.UTF8Encoding();
            byte[] keyByte = encoding.GetBytes(secretKey);

            HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);
            byte[] messageBytes = encoding.GetBytes(data);
            return Convert.ToBase64String(hmacsha256.ComputeHash(messageBytes));
        }

        private static string BuildDataToSign(IDictionary<string, string> paramsArray)
        {
            string[] signedFieldNames = paramsArray["signed_field_names"].Split(',');
            IList<string> dataToSign = new List<string>();

            foreach (string signedFieldName in signedFieldNames)
            {
                dataToSign.Add(signedFieldName + "=" + paramsArray[signedFieldName]);
            }

            return CommaSeparate(dataToSign);
        }

        private static string CommaSeparate(IList<string> dataToSign)
        {
            return string.Join(",", dataToSign);
        }
    }
}
