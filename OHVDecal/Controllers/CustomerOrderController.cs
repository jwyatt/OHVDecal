﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OHVDecal.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using OHVDecal.Helper;

namespace OHVDecal.Controllers
{
    public class CustomerOrderController : Controller
    {
        private readonly OHVDecalContext _context;
        private readonly IConfiguration configuration;
        private readonly CyberSourceHelper _cybersourceHelper;

        public CustomerOrderController(OHVDecalContext context, IConfiguration configuration,  IOptions<CyberSourceHelper> helper)
        {
            _context = context;
            this.configuration = configuration;
            this._cybersourceHelper = helper.Value;
        }

        [HttpGet]
        public IActionResult CreateOrder()
        {
            return View();
        }
                

        [HttpPost]
        public IActionResult PayForOrder(int NumberOfTags)
        {
            decimal calcAmount = NumberOfTags * 30;
            string amount = string.Format("{0:#.00}", calcAmount );

            DateTime refDatetime = DateTime.Now;
            string referenceNumber = "";
            referenceNumber += refDatetime.Year.ToString("0000");
            referenceNumber += refDatetime.Month.ToString("00");
            referenceNumber += refDatetime.Day.ToString("00");
            referenceNumber += refDatetime.Minute.ToString("00");
            referenceNumber += refDatetime.Second.ToString("00");
            referenceNumber += refDatetime.Millisecond.ToString("000");

            IDictionary<string, string> parameters = new Dictionary<string, string>();
            //dev keys
            //parameters.Add("access_key", "07f049d249e03218be56d7e10de3eb06");
            //parameters.Add("profile_id", "C746B057-3507-42F9-9544-352FD81097B4");

            parameters.Add("secret_key", _cybersourceHelper.SecretKey);
            parameters.Add("access_key", _cybersourceHelper.AccessKey);
            parameters.Add("profile_id", _cybersourceHelper.ProfileId);

            //prod keys
            //parameters.Add("access_key", "82b4a9e6c33934aabb49d29ce12aaed9");
            //parameters.Add("profile_id", "10BC4003-41BC-493B-8A90-EF7332F1C373");

            string merchCode = "OhvDecal-" + CreateGuid();
            parameters.Add("transaction_uuid", merchCode);//unique id for cybersource every transaction(?).
            parameters.Add("signed_field_names", "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,consumer_id,merchant_defined_data1,merchant_defined_data2");
            parameters.Add("unsigned_field_names", "");
            parameters.Add("signed_date_time", GetUTCDateTime());
            parameters.Add("locale", "en");
            parameters.Add("transaction_type", "sale");
            parameters.Add("reference_number", merchCode);   //referenceNumber); //Payments Merch Ref number ohv+guid instead of number
            parameters.Add("amount", amount);
            parameters.Add("currency", "USD");
            parameters.Add("consumer_id", "1263456");//portal account
            parameters.Add("merchant_defined_data1", "OHV Decal");
            parameters.Add("merchant_defined_data2", NumberOfTags.ToString());

            ViewBag.parameters = parameters;
            ViewBag.NumberOfTags = NumberOfTags;

            return View(_cybersourceHelper);
        }

        public string CreateGuid()
        {
            return Guid.NewGuid().ToString();
        }

        public string GetUTCDateTime()
        {
            DateTime time = DateTime.Now.ToUniversalTime();
            return time.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
        }
    }
}