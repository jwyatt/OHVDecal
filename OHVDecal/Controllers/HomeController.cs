﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OHVDecal.Models;
using OHVDecal.ViewModels;

namespace OHVDecal.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult DecalInformation()
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult DecalInformation(RequiredCheckbox requiredCheckbox)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(requiredCheckbox);
        //    }
        //    return RedirectToAction("CreateOrder", "CustomerOrder");           
        //}

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult test()
        {
            OHVDecalContext db = new OHVDecalContext();
            LogInfo log = new LogInfo();
            log.Info = "Test " + DateTime.Now;
            db.Add(log);
            db.SaveChanges();
            return View();
        }
    }
}
