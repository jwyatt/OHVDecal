﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OHVDecal.Models;
using Microsoft.Extensions.Configuration;
using OHVDecal.Helper;

namespace OHVDecal.Controllers
{
    public class ReplyController : Controller
    {
        private readonly OHVDecalContext _context;
        private readonly PaymentsContext _contextPayments; // = new PaymentsContext();
        //private IConfiguration _configuration;
        //private readonly CyberSourceHelper _helper;

        int numTags;

        public ReplyController(OHVDecalContext context, PaymentsContext paymentsContext) //, CyberSourceHelper helper)
        {
            _context = context;
            _contextPayments = paymentsContext;

            //_helper = helper;
            //_configuration = configuration;
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult SaveData()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            try
            {
                LogInfo logInfo = new LogInfo();
                string responseInfo = "response received: " + DateTime.Now.ToString() + "|";

                foreach (var item in Request.Form)
                {
                    string encodedValue = item.Value;
                    parameters.Add(item.Key, encodedValue);
                    responseInfo += "key:" + item.Key + " value:" + item.Value + "|";
                }
                //prod  
                //if (parameters["req_access_key"] != "82b4a9e6c33934aabb49d29ce12aaed9" || parameters["req_profile_id"] != "10BC4003-41BC-493B-8A90-EF7332F1C373")

                //dev 
                //if (parameters["req_access_key"] != "07f049d249e03218be56d7e10de3eb06" || parameters["req_profile_id"] != "C746B057-3507-42F9-9544-352FD81097B4")

                //if (parameters["req_access_key"] != _configuration.GetValue<string>("AccessKey") || parameters["req_profile_id"] != _configuration.GetValue<string>("ProfileId"))
                if (parameters["req_access_key"] != parameters["req_access_key"] || parameters["req_profile_id"] != parameters["req_profile_id"])
                //if (parameters["req_access_key"] != _helper.AccessKey  || parameters["req_profile_id"] != _helper.ProfileId)
                {
                    //logInfo.Info = string.Format("req_access_key::{0} | AccessKey::{1}", parameters["req_access_key"].ToString(), _helper.AccessKey);
                    //_context.Add(logInfo);
                    //_context.SaveChanges();

                    logInfo.Info = "INVALID req_access_key:: " + responseInfo;
                    _context.Add(logInfo);
                    _context.SaveChanges();
                    return Ok();
                }
                else
                {
                    logInfo.Info = responseInfo;
                    _context.Add(logInfo);
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                LogInfo logInfo = new LogInfo();
                logInfo.Info = "SaveData: " + e.Message;
                _context.Add(logInfo);
                _context.SaveChanges();
                return Ok();
            }

            if (parameters["decision"] != "ACCEPT") //customer canceled order
            {
                return Ok();
            }



            Reply reply = SaveReply(parameters);

            Sale sale = CreateSale(reply);
            CreateItem(sale);

            return Ok();
        }


        Reply SaveReply(Dictionary<string, string> parameters)
        {
            Reply reply = new Reply();
            try
            {
                //Sale
                reply.ReqAmount = parameters["req_amount"];
                reply.MerchantDefinedData1 = parameters["req_merchant_defined_data1"];
                reply.MerchantDefinedData2 = parameters["req_merchant_defined_data2"];
                reply.SignedDateTime = parameters["signed_date_time"];
                reply.AuthTransRefNo = parameters["auth_trans_ref_no"];
                reply.TransactionId = parameters["transaction_id"];
                reply.ReqTransactionUuid = parameters["req_transaction_uuid"];
                reply.ReqAmount = parameters["req_amount"];
                reply.ReqReferenceNumber = parameters["req_reference_number"]; //TODO goes to Sale OrderNumber

                //Billing
                reply.ReqBillToForename = parameters["req_bill_to_forename"];
                reply.ReqBillToSurname = parameters["req_bill_to_surname"];
                reply.ReqBillToAddressLine1 = parameters["req_bill_to_address_line1"];
                if (parameters.ContainsKey("req_bill_to_address_line2"))//optional field
                {
                    reply.ReqBillToAddressLine2 = parameters["req_bill_to_address_line2"];
                }
                reply.ReqBillToAddressState = parameters["req_bill_to_address_state"];
                reply.ReqBillToAddressCity = parameters["req_bill_to_address_city"];
                reply.ReqBillToAddressPostalCode = parameters["req_bill_to_address_postal_code"];
                reply.ReqBillToAddressCountry = parameters["req_bill_to_address_country"];
                reply.ReqBillToEmail = parameters["req_bill_to_email"];
                if (parameters.ContainsKey("req_bill_to_phone"))//optional field
                {
                    reply.ReqBillToPhone = parameters["req_bill_to_phone"];
                }

                //Shipping
                reply.ReqShipToForename = parameters["req_ship_to_forename"];
                reply.ReqShipToSurname = parameters["req_ship_to_surname"];
                reply.ReqShipToAddressLine1 = parameters["req_ship_to_address_line1"];
                if (parameters.ContainsKey("req_ship_to_address_line2"))//optional field
                {
                    reply.ReqShipToAddressLine2 = parameters["req_ship_to_address_line2"];
                }
                reply.ReqShipToAddressState = parameters["req_ship_to_address_state"];
                reply.ReqShipToAddressCity = parameters["req_ship_to_address_city"];
                reply.ReqShipToAddressPostalCode = parameters["req_ship_to_address_postal_code"];
                if (parameters.ContainsKey("req_ship_to_address_country")) //optional field
                {
                    reply.ReqShipToAddressCountry = parameters["req_ship_to_address_country"];
                }
                _context.Add(reply);
                _context.SaveChanges();

                numTags = Convert.ToInt32(reply.MerchantDefinedData2);
            }
            catch (Exception e)
            {
                LogInfo logInfo = new LogInfo();
                logInfo.Info = "SaveReply: " + e.Message;
                _context.Add(logInfo);
                _context.SaveChanges();
            }

            return reply;
        }

        Sale CreateSale(Reply reply)
        {
            Sale sale = new Sale();
            try
            {
                sale.Date = Convert.ToDateTime(reply.SignedDateTime);
                sale.AuthReconciliationId = reply.AuthTransRefNo;
                sale.CaptureReconciliationId = "";
                sale.RequestId = reply.TransactionId;
                sale.MerchantReferenceCode = reply.ReqTransactionUuid;
                sale.TotalPurchase = Convert.ToDecimal(reply.ReqAmount);
                sale.BatchAgency = "GFD";
                sale.OrderNumber = CreateOrderNumber(); //reply.ReqReferenceNumber;//merchant unique id to keep track of this order in database
                sale.DocAgency = "GFD";
                //sale.Sent = false; //set in database

                _contextPayments.Add(sale);
                _contextPayments.SaveChanges();
            }
            catch (Exception e)
            {
                LogInfo logInfo = new LogInfo();
                logInfo.Info = "CreateSale: " + e.Message + "  " + e.InnerException;
                _context.Add(logInfo);
                _context.SaveChanges();
            }

            return sale;
        }

        void CreateItem(Sale sale)
        {
            Items item;
            try
            {
                //$25 goes to OHV function 25067
                item = new Items();
                item.SaleId = sale.SaleId;
                item.Pca = "92000";
                item.CompObject = "4416";
                item.AgencyObject = "4411";
                item.Gaoindex = "25067";
                item.Agency = "GFD";
                item.Description = "OHV Decals Non Resident";
                item.ProductCode = "OHVNRDCL";
                item.UnitCost = 25.00M;
                item.Quantity = numTags;
                _contextPayments.Add(item);
                _contextPayments.SaveChanges();

                //$5 goes to G & F function 25068
                item = new Items();
                item.SaleId = sale.SaleId;
                item.Pca = "95000";
                item.CompObject = "4416";
                item.AgencyObject = "4411";
                item.Gaoindex = "25068";
                item.Agency = "GFD";
                item.Description = "OHV ADM Decals Non Resident";
                item.ProductCode = "OHVNRADM";
                item.UnitCost = 5.00M;
                item.Quantity = numTags;
                _contextPayments.Add(item);
                _contextPayments.SaveChanges();

            }
            catch (Exception e)
            {
                LogInfo logInfo = new LogInfo();
                logInfo.Info = "CreateItem: " + e.Message + "  " + e.InnerException;
                _context.Add(logInfo);
                _context.SaveChanges();
            }
        }


        string CreateOrderNumber()
        {
            DateTime refDatetime = DateTime.Now;
            string referenceNumber = "";
            referenceNumber += refDatetime.Year.ToString("0000");
            referenceNumber += refDatetime.Month.ToString("00");
            referenceNumber += refDatetime.Day.ToString("00");
            referenceNumber += refDatetime.Minute.ToString("00");
            referenceNumber += refDatetime.Second.ToString("00");
            referenceNumber += refDatetime.Millisecond.ToString("000");
            return referenceNumber;
        }

        string CreateGuid()
        {
            return Guid.NewGuid().ToString();
        }

        string GetUTCDateTime()
        {
            DateTime time = DateTime.Now.ToUniversalTime();
            return time.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
        }

        public string RemoveSpecialWords(string input)
        {
            string output = "";
            input.Replace("html", "~");
            input.Replace("http:", "~");
            input.Replace("https:", "~");
            input.Replace("fpt:", "~");

            return output;
        }
    }
}
